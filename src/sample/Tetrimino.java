package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.*;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

public class Tetrimino {
	
	public static List<Pane> Generate(int width, int height, int tab[][],int number){
		
		//Random generator = new Random();
		
		List<Pane> tetrimino = new ArrayList<>();
		
		for(int i=0;i<4;i++) {
			Pane p = new Pane();
			p.setPrefWidth(width/10);
			p.setPrefHeight(height/20);
			tetrimino.add(p);
		}
		
		switch(number) {
			case 0:
				tetrimino.get(0).setLayoutX(width/10*4);
				tetrimino.get(0).setLayoutY(0);
				tetrimino.get(2).setLayoutX(width/10*4);
				tetrimino.get(2).setLayoutY(height/20);
				tetrimino.get(1).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutY((height/20)*2);
				tetrimino.get(3).setLayoutX(width/10*4);
				tetrimino.get(3).setLayoutY((height/20)*3);
				for(Pane p: tetrimino) {
					Rectangle rectangleOut = new Rectangle(width/10, height/20, Color.RED);
					rectangleOut.setStroke(Color.BLACK);
					rectangleOut.setArcHeight(4);
					rectangleOut.setArcWidth(4);
					Line line1 = new Line(0 , 0, rectangleOut.getWidth(), rectangleOut.getHeight());
					Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
					//System.out.println(((Pane) p).getTranslateX() + " " + p.getHeight());
					Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.RED);
					rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
					rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
					rectangleIn.setStroke(Color.BLACK);
					rectangleIn.setArcHeight(4);
					rectangleIn.setArcWidth(4);
					p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				}
				
				
				
				tab[0][0] = 5; tab[0][1] = 1;
				tab[2][0] = 5; tab[2][1] = 2;
				tab[1][0] = 5; tab[1][1] = 3;
				tab[3][0] = 5; tab[3][1] = 4;
			
				break;
				
			case 1:
				tetrimino.get(2).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutX(width/10*3);
				tetrimino.get(0).setLayoutX(width/10*5);
				tetrimino.get(3).setLayoutX(width/10*4);
				tetrimino.get(3).setLayoutY(height/20);
				for(Pane p: tetrimino) {
					//((Pane) p).setBackground(new Background(new BackgroundFill(Color.SILVER, CornerRadii.EMPTY, Insets.EMPTY)));
					Rectangle rectangleOut = new Rectangle(width/10, height/20, Color.SILVER);
					rectangleOut.setStroke(Color.BLACK);
					rectangleOut.setArcHeight(4);
					rectangleOut.setArcWidth(4);
					Line line1 = new Line(0 , 0, rectangleOut.getWidth(), rectangleOut.getHeight());
					Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
					//System.out.println(((Pane) p).getTranslateX() + " " + p.getHeight());
					Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.SILVER);
					rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
					rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
					rectangleIn.setStroke(Color.BLACK);
					rectangleIn.setArcHeight(4);
					rectangleIn.setArcWidth(4);
					p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				}
				
				
				tab[2][0] = 5; tab[2][1] = 1;
				tab[1][0] = 4; tab[1][1] = 1;
				tab[0][0] = 6; tab[0][1] = 1;
				tab[3][0] = 5; tab[3][1] = 2;
				
				break;
				
			case 2:
				tetrimino.get(0).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutX(width/10*5);
				tetrimino.get(2).setLayoutX(width/10*4);
				tetrimino.get(2).setLayoutY(height/20);
				tetrimino.get(3).setLayoutX(width/10*5);
				tetrimino.get(3).setLayoutY(height/20);
				for(Pane p: tetrimino) {
					//((Pane) p).setBackground(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
					Rectangle rectangleOut = new Rectangle(width/10, height/20, Color.BLUE);
					rectangleOut.setStroke(Color.BLACK);
					rectangleOut.setArcHeight(4);
					rectangleOut.setArcWidth(4);
					Line line1 = new Line(0 , 0, rectangleOut.getWidth(), rectangleOut.getHeight());
					Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
					//System.out.println(((Pane) p).getTranslateX() + " " + p.getHeight());
					Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.BLUE);
					rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
					rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
					rectangleIn.setStroke(Color.BLACK);
					rectangleIn.setArcHeight(4);
					rectangleIn.setArcWidth(4);
					p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				}
				
				
				tab[0][0] = 5; tab[0][1] = 1;
				tab[1][0] = 6; tab[1][1] = 1;
				tab[2][0] = 5; tab[2][1] = 2;
				tab[3][0] = 6; tab[3][1] = 2;
			
				break;
				
			case 3:
				tetrimino.get(0).setLayoutX(width/10*4);
				tetrimino.get(2).setLayoutX(width/10*4);
				tetrimino.get(2).setLayoutY(height/20);
				tetrimino.get(1).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutY(height/20*2);
				tetrimino.get(3).setLayoutX(width/10*5);
				tetrimino.get(3).setLayoutY(height/20*2);
				for(Pane p: tetrimino) {
					//((Pane) p).setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
					Rectangle rectangleOut = new Rectangle(width/10, height/20, Color.YELLOW);
					rectangleOut.setStroke(Color.BLACK);
					rectangleOut.setArcHeight(4);
					rectangleOut.setArcWidth(4);
					Line line1 = new Line(0 , 0, rectangleOut.getWidth(), rectangleOut.getHeight());
					Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
					//System.out.println(((Pane) p).getTranslateX() + " " + p.getHeight());
					Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.YELLOW);
					rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
					rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
					rectangleIn.setStroke(Color.BLACK);
					rectangleIn.setArcHeight(4);
					rectangleIn.setArcWidth(4);
					p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				}
				
				
				tab[0][0] = 5; tab[0][1] = 1;
				tab[2][0] = 5; tab[2][1] = 2;
				tab[1][0] = 5; tab[1][1] = 3;
				tab[3][0] = 6; tab[3][1] = 3;
		
				break;
				
			case 4:
				tetrimino.get(0).setLayoutX(width/10*4);
				tetrimino.get(2).setLayoutX(width/10*4);
				tetrimino.get(2).setLayoutY(height/20);
				tetrimino.get(1).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutY(height/20*2);
				tetrimino.get(3).setLayoutX(width/10*3);
				tetrimino.get(3).setLayoutY(height/20*2);
				for(Pane p: tetrimino) {
					//((Pane) p).setBackground(new Background(new BackgroundFill(Color.PURPLE, CornerRadii.EMPTY, Insets.EMPTY)));
					Rectangle rectangleOut = new Rectangle(width/10, height/20, Color.PURPLE);
					rectangleOut.setStroke(Color.BLACK);
					rectangleOut.setArcHeight(4);
					rectangleOut.setArcWidth(4);
					Line line1 = new Line(0 , 0, rectangleOut.getWidth(), rectangleOut.getHeight());
					Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
					//System.out.println(((Pane) p).getTranslateX() + " " + p.getHeight());
					Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.PURPLE);
					rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
					rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
					rectangleIn.setStroke(Color.BLACK);
					rectangleIn.setArcHeight(4);
					rectangleIn.setArcWidth(4);
					p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				}
				
				
				tab[0][0] = 5; tab[0][1] = 1;
				tab[2][0] = 5; tab[2][1] = 2;
				tab[1][0] = 5; tab[1][1] = 3;
				tab[3][0] = 4; tab[3][1] = 3;
			
				break;
				
			case 5:
				tetrimino.get(0).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutX(width/10*5);
				tetrimino.get(3).setLayoutX(width/10*3);
				tetrimino.get(3).setLayoutY(height/20);
				tetrimino.get(2).setLayoutX(width/10*4);
				tetrimino.get(2).setLayoutY(height/20);
				for(Pane p: tetrimino) {
					//((Pane) p).setBackground(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
					Rectangle rectangleOut = new Rectangle(width/10, height/20, Color.NAVY);
					rectangleOut.setStroke(Color.BLACK);
					rectangleOut.setArcHeight(4);
					rectangleOut.setArcWidth(4);
					Line line1 = new Line(0 , 0, rectangleOut.getWidth(), rectangleOut.getHeight());
					Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
					//System.out.println(((Pane) p).getTranslateX() + " " + p.getHeight());
					Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.NAVY);
					rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
					rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
					rectangleIn.setStroke(Color.BLACK);
					rectangleIn.setArcHeight(4);
					rectangleIn.setArcWidth(4);
					p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				}
				
				
				tab[0][0] = 5; tab[0][1] = 1;
				tab[1][0] = 6; tab[1][1] = 1;
				tab[3][0] = 4; tab[3][1] = 2;
				tab[2][0] = 5; tab[2][1] = 2;
			
				break;
				
			case 6:
				tetrimino.get(0).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutX(width/10*5);
				tetrimino.get(2).setLayoutX(width/10*5);
				tetrimino.get(2).setLayoutY(height/20);
				tetrimino.get(3).setLayoutX(width/10*6);
				tetrimino.get(3).setLayoutY(height/20);
				for(Pane p: tetrimino) {
					//((Pane) p).setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
					Rectangle rectangleOut = new Rectangle(width/10, height/20, Color.GREEN);
					rectangleOut.setStroke(Color.BLACK);
					rectangleOut.setArcHeight(4);
					rectangleOut.setArcWidth(4);
					Line line1 = new Line(0 , 0, rectangleOut.getWidth(), rectangleOut.getHeight());
					Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
					//System.out.println(((Pane) p).getTranslateX() + " " + p.getHeight());
					Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.GREEN);
					rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
					rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
					rectangleIn.setStroke(Color.BLACK);
					rectangleIn.setArcHeight(4);
					rectangleIn.setArcWidth(4);
					p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				}
				
				
				tab[0][0] = 5; tab[0][1] = 1;
				tab[1][0] = 6; tab[1][1] = 1;
				tab[2][0] = 6; tab[2][1] = 2;
				tab[3][0] = 7; tab[3][1] = 2;
				
				break;
		}
		
		
		
		return tetrimino;
	}
	
	
	
	
}
