package sample;

import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class Playfield {
	
	public static Group setPlayfield(int width,int height) {
		
		Group group = new Group();
		
		for(int i=0;i<20;i++) {
			for(int j=0;j<10;j++) {
				
				Pane p = new Pane();
				p.setPrefWidth(width/10);
				p.setPrefHeight(height/20);
				p.setLayoutX(width/10*j);
				p.setLayoutY(height/20*i);
				p.setStyle("-fx-border-color: grey; -fx-background-color: black");
				//p.prefHeightProperty().bind(borderPane.heightProperty());
				//p.prefWidthProperty().bind(borderPane.widthProperty());
				group.getChildren().add(p);
				
			}
		}
		
		return group;
		
		
	}
	
}
