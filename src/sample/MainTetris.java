package sample;

import java.util.List;
import java.util.Random;

//import com.sun.jndi.url.iiopname.iiopnameURLContextFactory;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MainTetris extends Application {

	private boolean goRight = false;
	private boolean goLeft = false;
	private boolean goDown = false;
	private boolean slideDown = true;
	private boolean goRotate = false;
	private boolean move = true;

	//wymiary planszy
	private int height = 600;
	private int width = 500;

	//zmienne wyniku, linii, poziomu
	private int lines = 0;
	private int level = 1;
	private int score = 0;

	//liczniki
	private long lastUpdate = 0;
	private long lastSlide = 0;
	
	private int[][] plansza = new int[22][12];
	private int[][] positionXY = new int[4][2];
	private Pane[][] planszaKlocki = new Pane[22][12];
	
	private Random generator = new Random();
	private int generated;
	private int generatedCurr;
	private int rotateNumber=0;
	
	//generator klockow
	private List<Pane> tetrimino = null;
	private List<Pane> tetriminoNext = null;
	
	private Text tScore;
	private Text tLines;
	Button btnBackToMenu = new Button("BACK TO MENU");
	Font fText = new Font(22);
	@Override
	public void start(Stage stage) throws Exception {

		//menu
		StackPane bpMenu = new StackPane();
		bpMenu.setPrefWidth(width);
		bpMenu.setPrefHeight(height);
		Scene menu = new Scene(bpMenu, bpMenu.getPrefWidth(), bpMenu.getPrefHeight());
		stage.setResizable(false);

		/*
		//skalowanie
		Scale scale = new Scale(0, 0);
		scale.xProperty().bind(bpMenu.widthProperty().divide(width));
		scale.yProperty().bind(bpMenu.heightProperty().divide(height));

		bpMenu.getTransforms().add(scale);

		menu.rootProperty().addListener(new ChangeListener<Parent>() {
			@Override
			public void changed(ObservableValue<? extends Parent> arg0, Parent parent, Parent t1) {
				menu.rootProperty().removeListener(this);
				//scene.setRoot(borderPane);
				//((Region)t1).setPrefWidth(width);
				//((Region)t1).setPrefHeight(height);
				//borderPane.getChildren().clear();
				//borderPane.getChildren().add(t1);
				menu.rootProperty().addListener(this);
				menu.getRoot().getTransforms().setAll(scale);
			}
		});
		*/

		//tło menu
		for(int i = 0; i < 24; i++)
		{
			for(int j = 0; j < 20; j++)
			{
				Pane p = new Pane();
				p.setPrefWidth(bpMenu.getPrefWidth() / 20);
				p.setPrefHeight(bpMenu.getPrefHeight() / 24);
				p.setTranslateX(bpMenu.getPrefWidth() / 20 * j);
				p.setTranslateY(bpMenu.getPrefHeight() / 24 * i);
				Rectangle rectangleOut = new Rectangle(bpMenu.getPrefWidth()/20, bpMenu.getPrefHeight()/24, Color.GREY);
				rectangleOut.setStroke(Color.BLACK);
				rectangleOut.setArcHeight(4);
				rectangleOut.setArcWidth(4);
				Line line1 = new Line(0, 0, rectangleOut.getWidth(), rectangleOut.getHeight());
				Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
				Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.GREY);
				rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
				rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
				rectangleIn.setStroke(Color.BLACK);
				rectangleIn.setArcHeight(4);
				rectangleIn.setArcWidth(4);
				p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				//p.prefHeightProperty().bind(borderPane.heightProperty());
				//p.prefWidthProperty().bind(borderPane.widthProperty());
				bpMenu.getChildren().add(p);
			}
		}
		//CREDITS
		Rectangle rCredits = new Rectangle(width/20*16, height/24*8, Color.BLACK);
		rCredits.setTranslateY(height/24*4);
		Button btnBack = new Button("BACK");
		btnBack.setPrefWidth(width/20*4);
		btnBack.setPrefHeight(height/24*2);
		btnBack.setTranslateY(bpMenu.getPrefHeight()/24*10);
		btnBack.setFont(fText);
		btnBack.setStyle("-fx-background-color: black; -fx-text-fill: gray;");
		Text tCredits = new Text("Tomasz Markuszewski\n\nKuba Zając");
		tCredits.setTranslateY(width/20*4);
		tCredits.setFont(fText);
		tCredits.setStyle("-fx-fill: black; -fx-stroke: gray;");
		//T E T R I S - Logo

		Rectangle rLogo = new Rectangle(width/20*18, height/24*7, Color.BLACK);
		rLogo.setX(0);
		rLogo.setTranslateY(-(width/20*7.5));
		bpMenu.getChildren().add(rLogo);

		//T - Logo



		//Przyciski - Start
		Button btnStart = new Button("NEW GAME");
		btnStart.setPrefHeight(height/24*2);
		btnStart.setPrefWidth(width/20*8);
		btnStart.setTranslateY((bpMenu.getPrefWidth()/20*2));
		btnStart.setFont(fText);
		btnStart.setStyle("-fx-background-color: black; -fx-text-fill: gray;");
		bpMenu.getChildren().add(btnStart);

		//Przyciski - Exit
		Button btnExit = new Button("EXIT");
		btnExit.setPrefWidth(width/20*4);
		btnExit.setPrefHeight(height/24*2);
		btnExit.setTranslateY(bpMenu.getPrefHeight()/24*10);
		btnExit.setFont(fText);
		btnExit.setStyle("-fx-background-color: black; -fx-text-fill: gray;");
		bpMenu.getChildren().add(btnExit);

		//Przyciski - Credits
		Button btnCredits = new Button("CREDITS");
		btnCredits.setPrefWidth(width/20*6);
		btnCredits.setPrefHeight(height/24*2);
		btnCredits.setTranslateY(bpMenu.getPrefHeight()/24*6);
		btnCredits.setFont(fText);
		btnCredits.setStyle("-fx-background-color: black; -fx-text-fill: gray");
		bpMenu.getChildren().add(btnCredits);


		//Wyświetlenie
		stage.setScene(menu);
		stage.centerOnScreen();
		stage.show();

		//plansza
		for(int i=0;i<12;i++) plansza[0][i] = 1;
		for(int i=0;i<12;i++) plansza[21][i] = 1;
		
		for(int j=0;j<22;j++) plansza[j][0] = 1;
		for(int j=0;j<22;j++) plansza[j][11] = 1;
		
		//plan BorderPane(Panele top,right,bot,left,center)
		BorderPane borderPane = new BorderPane();
		borderPane.setPrefWidth(width);
		borderPane.setPrefHeight(height);

		// ustawienia wymiar�w poszeczeg�lnych paneli
		StackPane top = new StackPane();
		top.setMinHeight((borderPane.getPrefHeight()/24)*2);
		top.setPrefHeight((borderPane.getPrefHeight()/24)*2);
		for(int i=0;i<2;i++) {
			for (int j = 0; j < 20; j++) {
				Pane p = new Pane();
				p.setPrefWidth(borderPane.getPrefWidth() / 20);
				p.setPrefHeight(borderPane.getPrefHeight() / 24);
				p.setTranslateX(borderPane.getPrefWidth() / 20 * j);
				p.setTranslateY(borderPane.getPrefHeight() / 24 * i);
				Rectangle rectangleOut = new Rectangle(borderPane.getPrefWidth()/20, borderPane.getPrefHeight()/24, Color.GREY);
				rectangleOut.setStroke(Color.BLACK);
				rectangleOut.setArcHeight(4);
				rectangleOut.setArcWidth(4);
				Line line1 = new Line(0, 0, rectangleOut.getWidth(), rectangleOut.getHeight());
				Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
				Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.GREY);
				rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
				rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
				rectangleIn.setStroke(Color.BLACK);
				rectangleIn.setArcHeight(4);
				rectangleIn.setArcWidth(4);
				p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				top.getChildren().add(p);
			}
		}
	
		
		StackPane left = new StackPane();
		left.setMinHeight((height/24)*20);
		left.setMinWidth((width/20)*2);

		for(int i=0;i<20;i++) {
			for (int j = 0; j < 2; j++) {

				Pane p = new Pane();
				p.setPrefWidth(width / 20);
				p.setPrefHeight(height / 24);
				p.setTranslateX(width / 20 * j);
				p.setTranslateY(height / 24 * i);
				Rectangle rectangleOut = new Rectangle(width/20, height/24, Color.GREY);
				rectangleOut.setStroke(Color.BLACK);
				rectangleOut.setArcHeight(4);
				rectangleOut.setArcWidth(4);
				Line line1 = new Line(0, 0, rectangleOut.getWidth(), rectangleOut.getHeight());
				Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
				Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.GREY);
				rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
				rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
				rectangleIn.setStroke(Color.BLACK);
				rectangleIn.setArcHeight(4);
				rectangleIn.setArcWidth(4);
				p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				left.getChildren().add(p);
			}
		}
		
		StackPane right = new StackPane();
		right.setPrefWidth((borderPane.getPrefWidth()/20)*8);
		right.setPrefHeight((borderPane.getPrefHeight()/24)*20);
		for(int i=0;i<20;i++) {
			for (int j = 0; j < 8; j++) {
				Pane p = new Pane();
				p.setPrefWidth(borderPane.getPrefWidth() / 20);
				p.setPrefHeight(borderPane.getPrefHeight() / 24);
				p.setTranslateX(borderPane.getPrefWidth() / 20 * j);
				p.setTranslateY(borderPane.getPrefHeight() / 24 * i);
				Rectangle rectangleOut = new Rectangle(borderPane.getPrefWidth()/20, borderPane.getPrefHeight()/24, Color.GREY);
				rectangleOut.setStroke(Color.BLACK);
				rectangleOut.setArcHeight(4);
				rectangleOut.setArcWidth(4);
				Line line1 = new Line(0, 0, rectangleOut.getWidth(), rectangleOut.getHeight());
				Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
				Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.GREY);
				rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
				rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
				rectangleIn.setStroke(Color.BLACK);
				rectangleIn.setArcHeight(4);
				rectangleIn.setArcWidth(4);
				p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				right.getChildren().add(p);
			}
		}
		Pane pNext = new Pane();
		pNext.setPrefWidth(borderPane.getPrefWidth()/20*6);
		pNext.setPrefHeight(borderPane.getPrefHeight()/24*6);
		pNext.setTranslateX(borderPane.getPrefWidth()/20);
		pNext.setTranslateY(borderPane.getPrefHeight()/24*2);
		Pane pText = new Pane();
		pText.setPrefWidth(borderPane.getPrefWidth()/20*6);
		pText.setPrefHeight(borderPane.getPrefHeight()/24*8);
		pText.setTranslateX(borderPane.getPrefWidth()/20);
		pText.setTranslateY(borderPane.getPrefHeight()/24*11);
		
		Text tLevel;
		
		Pane rNext;
		Pane rNextCenter;
		rNext = new Pane();
		rNextCenter = new Pane();
		rNext.setStyle("-fx-border-color: grey; -fx-background-color: black");
		rNext.setPrefHeight((right.getPrefWidth()/4)*3);
		rNext.setPrefWidth((right.getPrefWidth()/4)*3);
		rNextCenter.setLayoutX((right.getPrefWidth()/16)*3);
		rNextCenter.setLayoutY((right.getPrefWidth()/16)*3);
		rNext.getChildren().add(rNextCenter);
		pNext.getChildren().add(rNext);
		Rectangle rText = new Rectangle((right.getPrefWidth()/4)*3, (right.getPrefHeight()/20)*8);
		rText.setStroke(Color.GREY);
		rText.setFill(Color.BLACK);
		pText.getChildren().add(rText);
		tScore = new Text("Score: " + score);
		tScore.setFont(fText);
		tScore.setStyle("-fx-fill: black; -fx-stroke: gray;");
		pText.getChildren().add(tScore);
		tScore.setTranslateX(pText.getPrefWidth()/6);
		tScore.setTranslateY(pText.getPrefHeight()/12*3);
		tLevel = new Text("Level : " + level);
		tLevel.setFont(fText);
		tLevel.setStyle("-fx-fill: black; -fx-stroke: gray;");
		pText.getChildren().add(tLevel);
		tLevel.setTranslateX(pText.getPrefWidth()/6);
		tLevel.setTranslateY(pText.getPrefHeight()/12*6);
		tLines = new Text( "Lines: " + lines);
		tLines.setFont(fText);
		tLines.setStyle("-fx-fill: black; -fx-stroke: gray;");
		pText.getChildren().add(tLines);
		tLines.setTranslateX(pText.getPrefWidth()/6);
		tLines.setTranslateY(pText.getPrefHeight()/12*9);
		right.getChildren().addAll(pNext, pText);
		
		StackPane bottom = new StackPane();
		bottom.setPrefHeight((borderPane.getPrefHeight()/24)*2);
		for(int i=0;i<2;i++) {
			for (int j = 0; j < 20; j++) {
				Pane p = new Pane();
				p.setPrefWidth(borderPane.getPrefWidth() / 20);
				p.setPrefHeight(borderPane.getPrefHeight() / 24);
				p.setTranslateX(borderPane.getPrefWidth() / 20 * j);
				p.setTranslateY(borderPane.getPrefHeight() / 24 * i);
				Rectangle rectangleOut = new Rectangle(borderPane.getPrefWidth()/20, borderPane.getPrefHeight()/24, Color.GREY);
				rectangleOut.setStroke(Color.BLACK);
				rectangleOut.setArcHeight(4);
				rectangleOut.setArcWidth(4);
				Line line1 = new Line(0, 0, rectangleOut.getWidth(), rectangleOut.getHeight());
				Line line2 = new Line(rectangleOut.getWidth(), 0, 0, rectangleOut.getHeight());
				Rectangle rectangleIn = new Rectangle((rectangleOut.getWidth()/10)*7, (rectangleOut.getHeight()/10)*7, Color.GREY);
				rectangleIn.setTranslateX((rectangleOut.getWidth()/20)*3);
				rectangleIn.setTranslateY((rectangleOut.getHeight()/20)*3);
				rectangleIn.setStroke(Color.BLACK);
				rectangleIn.setArcHeight(4);
				rectangleIn.setArcWidth(4);
				p.getChildren().addAll(rectangleOut, line1, line2, rectangleIn);
				bottom.getChildren().add(p);
			}
		}
		

		borderPane.getChildren().addAll(top, left, right, bottom);
		top.setTranslateX(0);
		top.setTranslateY(0);
		left.setTranslateX(0);
		left.setTranslateY(borderPane.getPrefHeight()/24*2);
		right.setTranslateX(borderPane.getPrefWidth()/20*12);
		right.setTranslateY(borderPane.getPrefHeight()/24*2);
		bottom.setTranslateX(0);
		bottom.setTranslateY(borderPane.getPrefHeight()/24*22);

		
		//Playfield to generato planszy - generujemy do root'a przekazujac wymiar szeroksc/wysokosc
		Group root = Playfield.setPlayfield((int)(borderPane.getPrefWidth()/20)*10, (int)(borderPane.getPrefHeight()/24)*20);
			

		// generujemy pierwszy klocek..
		generatedCurr = generator.nextInt(7);
		tetrimino = Tetrimino.Generate((int)(borderPane.getPrefWidth()/20)*10, (int)(borderPane.getPrefHeight()/24)*20, positionXY,generatedCurr);
		generated = generator.nextInt(7);
		tetriminoNext = TetriminoNext.Generate((int)(borderPane.getPrefWidth()/20)*10, (int)(borderPane.getPrefHeight()/24)*20,generated);
		for(int i=0;i<4;i++) {
			root.getChildren().add(tetrimino.get(i));
			rNextCenter.getChildren().add(tetriminoNext.get(i));
		}
		
		for(int i=0;i<4;i++) {
			for(int j=0;j<2;j++) System.out.println(positionXY[i][j]);
		}
		
		Rotate rotating = new Rotate((int)(borderPane.getPrefHeight()/24)*20, (int)(borderPane.getPrefWidth()/20)*10);
		
		borderPane.getChildren().add(root);
		root.setTranslateX(borderPane.getPrefWidth()/20*2);
		root.setTranslateY(borderPane.getPrefHeight()/24*2);
		//borderPane.setCenter(root);

        //Przycisk - Back to menu
        //Button btnBackToMenu = new Button("BACK TO MENU");
        btnBackToMenu.setPrefHeight(stage.getHeight()/24*2);
        btnBackToMenu.setPrefWidth(stage.getWidth()/20*8);
        btnBackToMenu.setTranslateY((stage.getWidth()/20*18));
        btnBackToMenu.setTranslateX((stage.getHeight()/24*6));
        btnBackToMenu.setFont(fText);
        btnBackToMenu.setStyle("-fx-background-color: black; -fx-text-fill: gray; -fx-border-color: gray;");
        Text tGameOver = new Text("G A M E   O V E R");
        tGameOver.setTranslateY(stage.getWidth()/20*6);
        tGameOver.setTranslateX(stage.getHeight()/24*4);
        Font fGameOver = new Font(44);
        tGameOver.setFont(fGameOver);
        tGameOver.setStyle("-fx-fill: black; -fx-stroke: red;");
        Pane q = new Pane();
        q.setPrefHeight(stage.getHeight());
        q.setPrefWidth(stage.getWidth());
        Rectangle gameOver = new Rectangle(stage.getWidth(), stage.getHeight(), Color.BLACK);
        q.getChildren().addAll(gameOver, tGameOver, btnBackToMenu);
        borderPane.getChildren().add(q);
        q.setVisible(false);
		
		Scene scene = new Scene(borderPane, borderPane.getPrefWidth(), borderPane.getPrefHeight());


		Scale scale2 = new Scale(0, 0);

		scale2.xProperty().bind(borderPane.widthProperty().divide(width));
		scale2.yProperty().bind(borderPane.heightProperty().divide(height));

		borderPane.getTransforms().add(scale2);

		scene.rootProperty().addListener(new ChangeListener<Parent>() {
			@Override
			public void changed(ObservableValue<? extends Parent> arg0, Parent parent, Parent t1) {
				scene.rootProperty().removeListener(this);
				scene.rootProperty().addListener(this);
				scene.getRoot().getTransforms().setAll(scale2);
			}
		});


		// event na klikniecie
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
            	//slideDown = false;
                switch (event.getCode()) {
                    case DOWN:  goDown = true; slideDown = false; break;
                    case LEFT:  goLeft = true; break;
                    case RIGHT: goRight = true; break;
                    case UP: goRotate = true; break;
                }
            }
        });
		
		// event na opuszczenie
		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
            	slideDown = true;
                switch (event.getCode()) {
                    case DOWN:  goDown = false; break;
                    case LEFT:  goLeft = false; break;
                    case RIGHT: goRight = false; break;
                    case UP: goRotate = false; break;
                }
            }
        });

		// obs�uga animacji
		AnimationTimer timer = new AnimationTimer() {
			 
			
            @Override
            public void handle(long now) {
            	
                int dx = 0, dy = 0;
                
                if(now - lastSlide >= 1_000_000_000 && slideDown) {
                	// automatyczny zjazd
                	
                	if(Check(positionXY)) {
                		for(int i=0;i<4;i++) {
		                	positionXY[i][1]++;	
		                	dy += height/24;
				            moveBlock(dx,dy,tetrimino.get(i));
				            dy = 0;
                		}
                	}
                	
                	lastSlide = now;
                }else {
                	
	                if (now - lastUpdate >= 5_0_000_000) {
	                	
	                	if(!Check(positionXY)) {
	                		
	                		Write(positionXY,tetrimino);
	                		//System.out.println("Wchodzi");
	                		

	                		///!!!! TU KASOWANIE
	                		// Problem w tym �e raz si� kasuj� wszystkie raz nie wszystkie

	                		Delete(root);

	                		for(int i=1;i<11;i++) {
	                			if(plansza[2][i]==1) {
	                			    System.out.println("TTT");
	                			    q.setVisible(true);
	                			    this.stop();
	                			    return;
	                			}
	                		}

	                		tetrimino = Tetrimino.Generate((width/20)*10, (height/24)*20, positionXY,generated);
	                		generatedCurr = generated;
	                		generated = generator.nextInt(7);
	                		tetriminoNext = TetriminoNext.Generate((int)(borderPane.getPrefWidth()/20)*10, (int)(borderPane.getPrefHeight()/24)*20,generated);
	                		rNextCenter.getChildren().clear();
	                		for(int i=0;i<4;i++) {
	                			root.getChildren().add(tetrimino.get(i));
	                			rNextCenter.getChildren().add(tetriminoNext.get(i));
	                		}
	                		
	                		rotateNumber = 0;
	                		
	                		return;
	                	}
	                	if (goRotate && Check(positionXY)) {
	                		System.out.println(rotateNumber);
	                		rotateNumber = rotateNumber % 4;
	                		if(rotating.rotate(tetrimino, generatedCurr, rotateNumber, positionXY, plansza)) rotateNumber++;
	                		
	                		System.out.println();
	                		for(int i=0;i<4;i++) {
	                			 System.out.println(i + ": " + positionXY[i][0] + " " +  positionXY[i][1]);
	                		}
	                		
	                		System.out.println("Po: " + rotateNumber);
		                }
		                if (goDown && Check(positionXY)) {
		                	for(int i=0;i<4;i++) {
		                		positionXY[i][1]++;	
		            		}
		                	System.out.println();
		                	for(int i=0;i<4;i++) {
	                			 System.out.println(i + ": " + positionXY[i][0] + " " +  positionXY[i][1]);
	                		}
		                	dy += height/24;
		                	lastSlide = now ;
		                }
		                if (goLeft && CheckLeft(positionXY) ) {
		                	for(int i=0;i<4;i++) {
		                		positionXY[i][0]--;	
		            		}
		                	dx -= width/20;
		                }
		                if (goRight && CheckRight(positionXY) ) {
		                	for(int i=0;i<4;i++) {
		                		positionXY[i][0]++;	
		            		}
		                	dx += width/20;
		                }
		                lastUpdate = now;
		                for(int i=0;i<4;i++) {
		                	moveBlock(dx,dy,tetrimino.get(i));	
	            		}
		                
	                }
                }
            }
        };
        //timer.start();

        // inicjalizacja stage'a
		btnStart.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.setScene(scene);
				stage.show();
				timer.start();
				stage.setResizable(true);
			}
		});

        //Przycisk Back To Menu
        btnBackToMenu.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.setScene(menu);
                stage.show();
                q.setVisible(false);
                for(int k = 1; k < 21; k++)
                {
                    for(int j = 1; j < 11; j++)
                    {
                        plansza[k][j] = 0;
                        root.getChildren().remove(planszaKlocki[k][j]);
                    }
                }
                root.getChildren().removeAll();
                tetrimino = Tetrimino.Generate((width/20)*10, (height/24)*20, positionXY,generated);
                generatedCurr = generated;
                generated = generator.nextInt(7);
                tetriminoNext = TetriminoNext.Generate((int)(borderPane.getPrefWidth()/20)*10, (int)(borderPane.getPrefHeight()/24)*20,generated);
                rNextCenter.getChildren().clear();
                for(int i=0;i<4;i++) {
                    root.getChildren().add(tetrimino.get(i));
                    rNextCenter.getChildren().add(tetriminoNext.get(i));
                }
                rotateNumber = 0;
                stage.setResizable(false);
            }
        });
		//Przycisk EXIT
		btnExit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});
		//przycisk CREDITS
		btnCredits.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				bpMenu.getChildren().addAll(rCredits, btnBack, tCredits);
				bpMenu.getChildren().removeAll(btnStart, btnCredits, btnExit);
			}
		});
		//przycisk BACK
		btnBack.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				bpMenu.getChildren().addAll(btnStart, btnCredits, btnExit);
				bpMenu.getChildren().removeAll(rCredits, btnBack, tCredits);
			}
		});
		//stage.setScene(scene);
		//stage.centerOnScreen();
		stage.setTitle("T E T R I S");
		//stage.show();
	}

	// poruszanie klocka
	public void moveBlock(int dx, int dy, Pane p) {
    	Translate translate = new Translate();
    	translate.setX(dx);
    	translate.setY(dy);
    	p.getTransforms().addAll(translate);
    	
    }

	
	// funkcje pomocnicze na obs�uge tablicy
	public boolean Check(int[][] tab) {
		
		for(int i=0;i<4;i++) {
       	 	if (plansza[tab[i][1]+1][tab[i][0]]!=0) {
       		 return false;
            }
		}
		
		return true;
		
	}
	
	public boolean CheckLeft(int[][] tab) {
		
		for(int i=0;i<4;i++) {
       	 	if (plansza[tab[i][1]][tab[i][0]-1]!=0) {
       		 return false;
            }
		}
		
		return true;
		
	}
	
	public boolean CheckRight(int[][] tab) {
		
		for(int i=0;i<4;i++) {
       	 	if (plansza[tab[i][1]][tab[i][0]+1]!=0) {
       		 return false;
            }
		}
		
		return true;
		
	}
	
	
	
	public void Write(int[][] tab, List<Pane> tetrimino) {
		
		for(int i=0;i<4;i++) {
       	 	plansza[tab[i][1]][tab[i][0]]=1;
       	 	planszaKlocki[tab[i][1]][tab[i][0]]=tetrimino.get(i);
		}
		
	}
	
	
	public  void Delete(Group root) {
		
			int combo = 1;
			for(int i=1;i<21;i++) {
				boolean cond = true;
				for(int j=1;j<11;j++) {
					if(plansza[i][j]==0) cond = false;
				}
				if(cond) {
					score = score + 10*combo;
					lines++;
						combo++;
							//for(int j=1;j<11;j++) {
									synchronized(this) {

										int ii = i;
										Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), new EventHandler<ActionEvent>() {

											private int jj = 0;
											@Override
											public void handle(ActionEvent event) {
												root.getChildren().remove(planszaKlocki[ii][jj]);
												System.out.println(root.getChildren().size());
												plansza[ii][jj]=0;
												jj++;
											}
										}));
										timeline.setCycleCount(11);
										timeline.playFromStart();
										timeline.setOnFinished(new EventHandler<ActionEvent>() {
											@Override
											public void handle(ActionEvent event) {
												for(int k = ii-1;k>-1;k--) {
													for(int j=1;j<11;j++) {
														if(planszaKlocki[k][j]!=null) {
															moveBlock(0, height/24, planszaKlocki[k][j]);
															plansza[k][j]=0;
															plansza[k+1][j]=1;
															planszaKlocki[k+1][j] = planszaKlocki[k][j];
															planszaKlocki[k][j] = null;
														}
													}
												}
											}
										});
									}
						    //}


				}
			}
			tScore.setText("Score: " + score);
			tLines.setText("Lines: " + lines);
	}
	

	public static void main(String[] args) {
		
	
		launch(args);
	}
}
