package sample;

import java.util.List;
import javafx.scene.layout.Pane;

public class RotateTable {
	
	
	
	public static void rotate(int tetriminoNumber,int rotateNumber,int tab[][]) {
		
		
		switch(tetriminoNumber) {
		
		case 0:
			
			switch((rotateNumber)%4) {
			
			
			case 0:
				
				tab[1][1]--;               
				tab[1][0]--;
				
				tab[0][1]++;
				tab[0][0]++;
				
				tab[3][1]--;
				tab[3][1]--;
				tab[3][0]--;
				tab[3][0]--;
				
				break;
				
			case 1:
				
				tab[1][1]--;               
				tab[1][0]++;
				
				tab[0][1]++;
				tab[0][0]--;
				
				tab[3][1]--;
				tab[3][1]--;
				tab[3][0]++;
				tab[3][0]++;
				
				break;
				
			case 2:
				
				tab[1][1]++;               
				tab[1][0]++;
				
				tab[0][1]--;
				tab[0][0]--;
				
				tab[3][1]++;
				tab[3][0]++;
				tab[3][1]++;
				tab[3][0]++;
				break;
				
			case 3:
				
				tab[1][1]++;               
				tab[1][0]--;
				
				tab[0][1]--;
				tab[0][0]++;
				
				tab[3][1]++;
				tab[3][0]--;
				tab[3][1]++;
				tab[3][0]--;
				break;
			
			}
			
			break;
			
		case 1:
			
			
			switch((rotateNumber)%4) {
			
			
			case 0:
				
				tab[1][0]++;               
				tab[1][1]--;
				
				tab[0][0]--;
				tab[0][1]++;
				
				tab[3][0]--;
				tab[3][1]--;
				
				break;
				
			case 1:
				
				tab[1][0]++;               
				tab[1][1]++;
				
				tab[0][0]--;
				tab[0][1]--;
				
				tab[3][0]++;
				tab[3][1]--;
				break;
				
			case 2:
				
				tab[1][0]--;               
				tab[1][1]++;
				
				tab[0][0]++;
				tab[0][1]--;
				
				tab[3][0]++;
				tab[3][1]++;
				break;
				
			case 3:
				
				tab[1][0]--;               
				tab[1][1]--;
				
				tab[0][0]++;
				tab[0][1]++;
				
				tab[3][0]--;
				tab[3][1]++;
				break;
			
			}
			
			
			break;
			
		case 2:
			
			switch((rotateNumber)%4) {
			
			
			case 0:
				
				tab[1][1]++;               
				tab[1][1]++;
				
				tab[0][0]++;
				tab[0][1]++;
				
				tab[3][0]--;
				tab[3][1]++;
				break;
				
			case 1:
				
				tab[1][0]--;               
				tab[1][0]--;
				
				tab[0][0]--;
				tab[0][1]++;
				
				tab[3][0]--;
				tab[3][1]--;
				break;
				
			case 2:
				
				tab[1][1]--;               
				tab[1][1]--;
				
				tab[0][0]--;
				tab[0][1]--;
				
				tab[3][0]++;
				tab[3][1]--;
				break;
				
			case 3:
				
				tab[1][0]++;               
				tab[1][0]++;
				
				tab[0][0]++;
				tab[0][1]--;
				
				tab[3][0]++;
				tab[3][1]++;
				break;
			
			}
			
				
			
			break;
			
		case 3:
			
			switch((rotateNumber)%4) {
			
			
			case 0:
				
				tab[1][1]--;               
				tab[1][0]--;
				
				tab[0][1]++;
				tab[0][0]++;
				
				tab[3][0]--;
				tab[3][0]--;
				
				break;
				
			case 1:
				
				tab[1][1]--;               
				tab[1][0]++;
				
				tab[0][1]++;
				tab[0][0]--;
				
				tab[3][1]--;
				tab[3][1]--;
				
				break;
				
			case 2:
				
				tab[1][1]++;               
				tab[1][0]++;
				
				tab[0][1]--;
				tab[0][0]--;
				
		
				tab[3][0]++;
				tab[3][0]++;
				break;
				
			case 3:
				
				tab[1][1]++;               
				tab[1][0]--;
				
				tab[0][1]--;
				tab[0][0]++;
				
				tab[3][1]++;
		
				tab[3][1]++;
				
				break;
			
			}
			
			break;
			
		case 4:
			
			switch((rotateNumber)%4) {
			
			case 0:
				
				tab[1][1]--;               
				tab[1][0]--;
				
				tab[0][1]++;
				tab[0][0]++;
				
				tab[3][1]--;
				tab[3][1]--;
				
				break;
				
			case 1:
				
				tab[1][1]--;               
				tab[1][0]++;
				
				tab[0][1]++;
				tab[0][0]--;
				
				tab[3][0]++;
				tab[3][0]++;
				
				break;
				
			case 2:
				
				tab[1][1]++;               
				tab[1][0]++;
				
				tab[0][1]--;
				tab[0][0]--;
				
		
				tab[3][1]++;
				tab[3][1]++;
				break;
				
			case 3:
				
				tab[1][1]++;               
				tab[1][0]--;
				
				tab[0][1]--;
				tab[0][0]++;
				
				tab[3][0]--;
		
				tab[3][0]--;
				
				break;
			
			}
			
			break;
			
		case 5:
			
			switch((rotateNumber)%4) {
			
			case 0:
				
				tab[1][1]++;               
				tab[1][1]++;
				
				tab[0][1]++;
				tab[0][0]++;
				
				tab[3][0]++;
				tab[3][1]--;
				
				break;
				
			case 1:
				
				tab[1][0]--;               
				tab[1][0]--;
				
				tab[0][1]++;
				tab[0][0]--;
				
				tab[3][1]++;
				tab[3][0]++;
				
				break;
				
			case 2:
				
				tab[1][1]--;               
				tab[1][1]--;
				
				tab[0][1]--;
				tab[0][0]--;
				
		
				tab[3][1]++;
				tab[3][0]--;
				break;
				
			case 3:
				
				tab[1][0]++;               
				tab[1][0]++;
				
				tab[0][1]--;
				tab[0][0]++;
				
				tab[3][1]--;
		
				tab[3][0]--;
				
				break;
			
			}
			
			break;
			
		case 6:
			
			switch((rotateNumber)%4) {
			
			case 0:
				
				tab[1][1]++;               
				tab[1][0]++;
				
				tab[0][0]++;
				tab[0][0]++;
				
				tab[3][1]++;
				tab[3][0]--;
				
				break;
				
			case 1:
				
				tab[1][1]++;               
				tab[1][0]--;
				
				tab[0][1]++;
				tab[0][1]++;
				
				tab[3][1]--;
				tab[3][0]--;
				
				break;
				
			case 2:
				
				tab[1][0]--;               
				tab[1][1]--;
				
				tab[0][0]--;
				tab[0][0]--;
				
		
				tab[3][1]--;
				tab[3][0]++;
				break;
				
			case 3:
				
				tab[1][1]--;               
				tab[1][0]++;
				
				tab[0][1]--;
				tab[0][1]--;
				
				tab[3][1]++;
		
				tab[3][0]++;
				
				break;
			
			}
			
			break;
			
		}
		
		
	}

}
