package sample;

import java.util.List;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class Rotate {
	
	int height;
	int width;
	
	public Rotate(int height, int width) {
		this.height = height;
		this.width = width;
	}
	
	public boolean rotate(List<Pane> tetrimino,int tetriminoNumber,int rotateNumber,int tab[][], int plansza[][]) {
		
		switch(tetriminoNumber) {
		
		case 0:
			
			switch(rotateNumber) {
			
			case 0:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       		return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 		System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 	return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*3, height/20);
				tetrimino.get(0).relocate(width/10*5, height/20);
				tetrimino.get(3).relocate(width/10*2, height/20);
				
				break;
				
			case 1:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 	return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, 0);
				tetrimino.get(0).relocate(width/10*4, height/20*2);
				tetrimino.get(3).relocate(width/10*4, height/-20);
					
				break;
				
			case 2:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, height/20);
				tetrimino.get(0).relocate(width/10*3, height/20);
				tetrimino.get(3).relocate(width/10*6, height/20);
				
				break;
				
			case 3:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, height/20*2);
				tetrimino.get(0).relocate(width/10*4, 0);
				tetrimino.get(3).relocate(width/10*4, height/20*3);
				
				break;
			
			}
			
			break;
			
		case 1:
			
			
			switch(rotateNumber) {
			
			
			case 0:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 		System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, height/-20);
				tetrimino.get(0).relocate(width/10*4, height/20);
				tetrimino.get(3).relocate(width/10*3, 0);
				
				break;
				
			case 1:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
	
				tetrimino.get(1).setLayoutX(width/10*5);
				tetrimino.get(1).setLayoutY(0);	
				tetrimino.get(0).setLayoutX(width/10*3);
				tetrimino.get(0).setLayoutY(0);
				tetrimino.get(3).setLayoutX(width/10*4);
				tetrimino.get(3).setLayoutY(height/-20);	
				break;
				
			case 2:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).setLayoutX(width/10*4);
				tetrimino.get(1).setLayoutY(height/20);
				tetrimino.get(0).setLayoutX(width/10*4);
				tetrimino.get(0).setLayoutY(height/-20);
				tetrimino.get(3).setLayoutX(width/10*5);
				tetrimino.get(3).setLayoutY(0);
				
				break;
				
			case 3:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).setLayoutX(width/10*3);
				tetrimino.get(1).setLayoutY(0);
				tetrimino.get(0).setLayoutX(width/10*5);
				tetrimino.get(0).setLayoutY(0);
				tetrimino.get(3).setLayoutX(width/10*4);
				tetrimino.get(3).setLayoutY(height/20);
				
				
				break;
			
			}
			
			
			break;
			
		case 2:
			
			switch(rotateNumber) {
			
			
			case 0:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 		System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, height/20*2);
				tetrimino.get(0).relocate(width/10*5, height/20);
				tetrimino.get(3).relocate(width/10*4, height/20*2);
				
				break;
				
			case 1:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
	
				tetrimino.get(1).relocate(width/10*3, height/20*2);
				tetrimino.get(0).relocate(width/10*4, height/20*2);
				tetrimino.get(3).relocate(width/10*3, height/20);	
				break;
				
			case 2:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*3, 0);
				tetrimino.get(0).relocate(width/10*3, height/20);
				tetrimino.get(3).relocate(width/10*4, 0);
				
				break;
				
			case 3:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, 0);
				tetrimino.get(0).relocate(width/10*4, 0);
				tetrimino.get(3).relocate(width/10*5, height/20);
				
				
				break;
			
			}
			
			break;
			
		case 3:
			
			switch(rotateNumber) {
			
			case 0:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       		return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 		System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 	return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*3, height/20);
				tetrimino.get(0).relocate(width/10*5, height/20);
				tetrimino.get(3).relocate(width/10*3, height/20*2);
				
				break;
				
			case 1:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 	return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, 0);
				tetrimino.get(0).relocate(width/10*4, height/20*2);
				tetrimino.get(3).relocate(width/10*3, 0);
					
				break;
				
			case 2:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, height/20);
				tetrimino.get(0).relocate(width/10*3, height/20);
				tetrimino.get(3).relocate(width/10*5, 0);
				
				break;
				
			case 3:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, height/20*2);
				tetrimino.get(0).relocate(width/10*4, 0);
				tetrimino.get(3).relocate(width/10*5, height/20*2);
				
				break;
			
			}
			
			break;
			
		case 4:
			
			switch(rotateNumber) {
			
			case 0:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       		return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 		System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 	return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*3, height/20);
				tetrimino.get(0).relocate(width/10*5, height/20);
				tetrimino.get(3).relocate(width/10*3, 0);
				
				break;
				
			case 1:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 	return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, 0);
				tetrimino.get(0).relocate(width/10*4, height/20*2);
				tetrimino.get(3).relocate(width/10*5, 0);
					
				break;
				
			case 2:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, height/20);
				tetrimino.get(0).relocate(width/10*3, height/20);
				tetrimino.get(3).relocate(width/10*5, height/20*2);
				
				break;
				
			case 3:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
					if(tab[i][1] < 0 || tab[i][1] > 21 || tab[i][0] < 0 || tab[i][0] > 11 ) {
						System.out.println("wchodzi");
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
			       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
			       	 return false;
					}
				}
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, height/20*2);
				tetrimino.get(0).relocate(width/10*4, 0);
				tetrimino.get(3).relocate(width/10*3, height/20*2);
				
				break;
			
			}
			
			break;
			
		case 5:
			
			switch(rotateNumber) {
			
			
			case 0:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 		System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, height/20*2);
				tetrimino.get(0).relocate(width/10*5, height/20);
				tetrimino.get(3).relocate(width/10*4, 0);
				
				break;
				
			case 1:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
	
				tetrimino.get(1).relocate(width/10*3, height/20*2);
				tetrimino.get(0).relocate(width/10*4, height/20*2);
				tetrimino.get(3).relocate(width/10*5, height/20);	
				break;
				
			case 2:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*3, 0);
				tetrimino.get(0).relocate(width/10*3, height/20);
				tetrimino.get(3).relocate(width/10*4, height/20*2);
				
				break;
				
			case 3:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, 0);
				tetrimino.get(0).relocate(width/10*4, 0);
				tetrimino.get(3).relocate(width/10*3, height/20);
				
				
				break;
			
			}
			
			break;
			
		case 6:
			
			switch(rotateNumber) {
			
			
			case 0:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 		System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*6, height/20);
				tetrimino.get(0).relocate(width/10*6, 0);
				tetrimino.get(3).relocate(width/10*5, height/20*2);
				
				break;
				
			case 1:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
	
				tetrimino.get(1).relocate(width/10*5, height/20*2);
				tetrimino.get(0).relocate(width/10*6, height/20*2);
				tetrimino.get(3).relocate(width/10*4, height/20);	
				break;
				
			case 2:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*4, height/20);
				tetrimino.get(0).relocate(width/10*4, height/20*2);
				tetrimino.get(3).relocate(width/10*5, 0);
				
				break;
				
			case 3:
				
				RotateTable.rotate(tetriminoNumber, rotateNumber, tab);
				
				for(int i=0;i<4;i++) {
		       	 	if (plansza[tab[i][1]][tab[i][0]]!=0) {
		       	 	System.out.println("wchodzi");
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+1, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+2, tab);
		       	 	RotateTable.rotate(tetriminoNumber, rotateNumber+3, tab);
		       	 return false;
		            }
				}
				
				tetrimino.get(1).relocate(width/10*5, 0);
				tetrimino.get(0).relocate(width/10*4, 0);
				tetrimino.get(3).relocate(width/10*6, height/20);
				
				
				break;
			
			}
			
			break;
			
		}
		return true;
		
		
	}

}
